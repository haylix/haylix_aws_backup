#!/bin/bash

echo "===================================================================================="
echo "Destroy"
echo "===================================================================================="

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

TERRADIR=${SCRIPTDIR}/../domain/${CLIENT}

cd ${TERRADIR}

read -p "Do you really want to destroy the stack for ${CLIENT}? " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    terraform init && terraform destroy -auto-approve
fi
