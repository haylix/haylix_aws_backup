#!/bin/bash

echo "===================================================================================="
echo "deploy"
echo "===================================================================================="

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

TERRADIR=${SCRIPTDIR}/../domain/${CLIENT}

cd ${TERRADIR}

terraform init && terraform apply -auto-approve
