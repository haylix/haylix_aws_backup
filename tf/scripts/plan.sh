#!/bin/bash

echo "===================================================================================="
echo "initiate, validate, and plan"
echo "===================================================================================="

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

TERRADIR=${SCRIPTDIR}/../domain/${CLIENT}

cd ${TERRADIR}

terraform init && terraform validate && terraform plan
