provider "aws" {
  region = "ap-southeast-2"
}

module "kms" {
  source = "../../modules/kms"

  aws_account_id = var.aws_account_id
  name           = "${var.application_name}-kms"
}

module "sns" {
  source = "../../modules/sns"

  name = "${var.application_name}-sns"
  tags = {
    cost_centre = "${var.cost_centre}"
    application = "${var.application_name}"
    Terraform   = true
  }
}

module "iam" {
  source = "../../modules/iam"

  name = "${var.application_name}-role"
  tags = {
    cost_centre = "${var.cost_centre}"
    application = "${var.application_name}"
    Terraform   = true
  }
}

module "backup" {
  source = "../../modules/backup"

  vault_name        = "${var.application_name}-vault"
  plan_name_01         = "${var.application_name}-prod-plan"
  plan_name_02         = "${var.application_name}-nonprod-plan"
  iam_role_arn      = module.iam.iam_role_arn
  vault_kms_key_arn = module.kms.kms_key_arn
  sns_topic_arn     = module.sns.sns_arn
  backup_vault_events = [
    "COPY_JOB_FAILED",
    "BACKUP_JOB_FAILED",
    "BACKUP_JOB_EXPIRED",
    "RESTORE_JOB_FAILED",
    "RECOVERY_POINT_MODIFIED",
    "BACKUP_PLAN_MODIFIED"
  ]

  tags = {
    cost_centre = "${var.cost_centre}"
    application = "${var.application_name}"
    Terraform   = true
  }
}

/*
TODO CloudWatch Dashboard, CloudWatch Alarm, and EventBridge module
*/
module "cloudwatch" {
  source = "../../modules/cloudwatch"

  name              = var.application_name
  sns_topic_arn     = module.sns.sns_arn
}

