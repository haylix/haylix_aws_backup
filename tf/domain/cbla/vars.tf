variable "aws_account_id" {
  type = string
}
variable "aws_region" {
  type = string
}
variable "application_name" {
  description = "Application name to be deployed"
  type        = string
}
variable "cost_centre" {
  description = "CostCentre Tag"
  type        = string
}