terraform {
  backend "s3" {
    bucket  = "apa-terraform"
    key     = "$aws-backup/terraform.tfstate"
    region  = "ap-southeast-2"
    encrypt = "true"
  }
  required_providers {
    aws = {
      source   = "hashicorp/aws"
      version  = "~> 3.0"
      /* TODO: usinghttps://registry.terraform.io/providers/hashicorp/aws/latest/docs#assume_role
      assume_role {
        role_arn = "arn:aws:iam::ACCOUNT_ID:role/ROLE_NAME"
      }*/
    }
  }
}
