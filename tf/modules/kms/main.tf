resource "aws_kms_key" "kms" {
    description         = "KMS Key for the ${var.name}"
    enable_key_rotation = "true"
    policy              =  <<POLICY
    {
        "Version": "2012-10-17",
        "Id": "${var.name}-policy",
        "Statement": [
        {
            "Sid" : "Enable IAM User Permissions",
            "Effect" : "Allow",
            "Principal" : {
            "AWS" : "arn:aws:iam::${var.aws_account_id}:root"
            },
            "Action" : "kms:*",
            "Resource" : "*"
        }
        ]
    }

POLICY

    lifecycle {
        prevent_destroy = false
    }
}

resource "aws_kms_alias" "kms" {
    name          = "alias/${var.name}"
    target_key_id = "${aws_kms_key.kms.key_id}"
}
