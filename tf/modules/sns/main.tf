resource "aws_sns_topic" "sns-notif-backup" {
    display_name = var.name
    name         = var.name

    tags =  var.tags
}

data "aws_iam_policy_document" "sns-iam-policy" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Publish",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }

    resources = [
      aws_sns_topic.sns-notif-backup.arn,
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_sns_topic_policy" "sns-topic-policy" {
  arn    = aws_sns_topic.sns-notif-backup.arn
  policy = data.aws_iam_policy_document.sns-iam-policy.json
}

resource "aws_sns_topic_subscription" "email-target" {
  topic_arn = aws_sns_topic.sns-notif-backup.arn
  protocol  = "email"
  endpoint  = "support@haylix.com"
}

