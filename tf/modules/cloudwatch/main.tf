resource "aws_cloudwatch_dashboard" "main" {
    dashboard_name = "${var.name}-dashboard"
    dashboard_body = <<EOF
    {
        "widgets": [
            {
                "type": "metric",
                "x": 0,
                "y": 0,
                "width": 12,
                "height": 6,
                "properties": {
                    "metrics": [
                        [ "AWS/Backup", "NumberOfBackupJobsCompleted" ],
                        [ ".", "NumberOfBackupJobsRunning" ],
                        [ ".", "NumberOfRecoveryPointsCompleted" ],
                        [ ".", "NumberOfBackupJobsCreated" ]
                    ],
                    "period": 300,
                    "stat": "Average",
                    "region": "ap-southeast-2",
                    "title": "${var.name}-Jobs"
                }
            }
        ]

    }
    EOF
}

resource "aws_cloudwatch_metric_alarm" "main" {
    alarm_name                = "${var.name}-alarm"
    comparison_operator       = "LessThanThreshold"
    evaluation_periods        = "1"
    metric_name               = "NumberOfBackupJobsFailed"
    namespace                 = "AWS/EC2"
    period                    = "3600"
    statistic                 = "Average"
    threshold                 = "1"
    datapoints_to_alarm       = "1"
    treat_missing_data        = "missing"
    alarm_description         = "The number of backup jobs that AWS Backup scheduled but did not start."
    actions_enabled           = "true"
    alarm_actions             = [var.sns_topic_arn]
}

resource "aws_cloudwatch_event_rule" "job-state-change" {
    name        = "${var.name}-Job-State-Change"
    description = "${var.name} Job's state changes"

    event_pattern = <<EOF
    {
    "source": ["aws.backup"],
    "detail-type": ["Backup Job State Change"]
    }
EOF
}

resource "aws_cloudwatch_event_target" "job-state-change-target" {
    rule      = aws_cloudwatch_event_rule.job-state-change.name
    target_id = "SendToSNS"
    arn       = var.sns_topic_arn
}
