variable "vault_name" {}
variable "plan_name_01" {}
variable "plan_name_02" {}
variable "vault_kms_key_arn" {}
variable "iam_role_arn" {}
variable "sns_topic_arn" {}
variable "backup_vault_events" {
    type = list
}
variable "tags" {
    description = "A mapping of tags to assign to the resource"
    type        = map(string)
    default     = {}
}