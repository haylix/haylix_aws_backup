resource "aws_backup_vault" "backup_vault" {
    name        = var.vault_name
    kms_key_arn = var.vault_kms_key_arn
    tags        = var.tags
}

resource "aws_backup_plan" "backup_plan_prod" {
    name = var.plan_name_01
    /* TODO: using dynamic Blocks for multi rule:
    https://www.terraform.io/docs/language/expressions/dynamic-blocks.html */
    rule {
        rule_name         = "DailyBackup-Rule-Prod"
        target_vault_name = aws_backup_vault.backup_vault.name
        schedule          = "cron(0 12 ? * * *)"
        start_window = 120
        completion_window = 180
        recovery_point_tags = {
            Backup-by = "DailyBackup-Rule"
        } 
        lifecycle {
            delete_after = 35
        }
    }
    advanced_backup_setting {
        backup_options = {
            WindowsVSS = "enabled"
        }
        resource_type = "EC2"
    }
    tags        = var.tags
}

resource "aws_backup_selection" "daily_prod" {
    name = "daily-selection-prod"
    iam_role_arn = var.iam_role_arn
    plan_id = aws_backup_plan.backup_plan_prod.id
    selection_tag {
        type  = "STRINGEQUALS"
        key   = "aws-daily-backup"
        value = "enabled"
    }
    selection_tag {
        type  = "STRINGEQUALS"
        key   = "env"
        value = "prod"
    }
}

resource "aws_backup_plan" "backup_plan_nonprod" {
    name = var.plan_name_02
    /* TODO: using dynamic Blocks for multi rule:
    https://www.terraform.io/docs/language/expressions/dynamic-blocks.html */
    rule {
        rule_name         = "DailyBackup-Rule-Nonprod"
        target_vault_name = aws_backup_vault.backup_vault.name
        schedule          = "cron(0 17 ? * * *)"
        start_window = 120
        completion_window = 180
        recovery_point_tags = {
            Backup-by = "DailyBackup-Rule"
        }
        lifecycle {
            delete_after = 35
        }
    }
    advanced_backup_setting {
        backup_options = {
            WindowsVSS = "enabled"
        }
        resource_type = "EC2"
    }
    tags        = var.tags
}

resource "aws_backup_selection" "daily_nonprod" {
    name = "daily-selection-nonprod"
    iam_role_arn = var.iam_role_arn
    plan_id = aws_backup_plan.backup_plan_nonprod.id

    condition {
      string_equals {
         key = "aws:ResourceTag/aws-daily-backup"
         value = "enabled"
      }
      string_not_equals {
         key = "aws:ResourceTag/env"
         value = "prod"
      }
    }

   resources = ["*"]
}

resource "aws_backup_vault_notifications" "notifications" {
    backup_vault_name = aws_backup_vault.backup_vault.name
    sns_topic_arn = var.sns_topic_arn
    backup_vault_events = ["BACKUP_JOB_FAILED", "RESTORE_JOB_FAILED", "COPY_JOB_FAILED", "S3_RESTORE_OBJECT_FAILED"]
}