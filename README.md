# Haylix AWS Backup Solution

This repository contains CloudFormation stack for [AWS Backup](##AWS-Backup) and [Data Life Cycle Manager](##Data-Life-Cycle).

## AWS Backup

As part of AWS Backup, the following resources are being created:

1. {aws-account-id}-backup-vault: which is an encrypted container to organise the backups. For more information click [here](https://docs.aws.amazon.com/aws-backup/latest/devguide/vaults.html).

2. {aws-account-id}-backup-plan: is a policy based definition express when and how the backup occurs. Click [here](https://docs.aws.amazon.com/aws-backup/latest/devguide/about-backup-plans.html) for more details.

3. Backup Rules: the backup plans is composed of daily, weekly, and monthly rules. Each backup rule defines the frequency, window frame, lifecycle, and its vault.

4. Backup Assignments: assigns resources to one or more rule via tag. This can be used to classify the data to which rule to be assigned.

5. SNS Topic: to send notifications for the following events:
    - COPY_JOB_FAILED
    - BACKUP_JOB_FAILED
    - BACKUP_JOB_EXPIRED
    - RESTORE_JOB_FAILED
    - RECOVERY_POINT_MODIFIED
    - BACKUP_PLAN_MODIFIED

6. KMS: to encrypt the backup vault.
7. Reporting Feature to be added([sample](https://aws.amazon.com/blogs/storage/amazon-cloudwatch-events-and-metrics-for-aws-backup/))

## Data Life Cycle

As part of data life cycle stack, the following resources are being created:

1. data-lifecycle-manager-role:
2. NonProdLifecyclePolicy:
3. ProdLifecyclePolicy:
